# GefCom2012_attempt

pues eso, un repo pa' probar como de buenos podemos llegar a ser con el kagle del Gefcom2012 de wind forecasting.

A partir de aqui la info que aparece en el kagle.

# EXPLICACION DE LOS DATOS 
"train.csv" contains the training data:

- the first column ("date") is a timestamp giving date and time of the hourly wind power measurements in following columns. For instance "2009070812" is for the 8th of July 2009 at 12:00;

- the following 7 columns ("wp1" to "wp7") gather the normalized wind power measurements for the 7 wind farms. They are normalized so as to take values between 0 and 1 in order for the wind farms not to be recognizable.

In parallel, files with explanatory variables (wind forecasts) are also provided for those who may want to use them. For example, the file "windforecasts_wf1" contains the wind forecasts for the wind farm 1. In these files:

- the first column ("date") is a timestamp giving date and time at which the forecasts are issued. For instance "2009070812" is for the 8th of July 2009 at 12:00;

- the second column ("hors") is for the lead time of the forecast. For instance if "date" = 2009070812 and "hors" = 1, the forecast is for the 8th of July 2009 at 13:00

- the following 4 columns ("u", "v", "ws" and "wd") are the forecasts themselves, the first two being the zonal and meridional wind components, while the following two are corresponding wind speed and direction.

Finally, the file "benchmark.csv" provide example forecast results from the persistence forecast method ("what you see is what you get"). This file also gives a template for submission of results that should be strictly followed.

Please notice that the first column of "benchmark.csv" is called "id" and contains unique identifier for each row. The other 8 columns are the same as "train.csv". When submitting the results, please make sure that your file includes totally 9 columns in the same format as "benchmark.csv".

# DETALLES A TENER EN CUENTA


The topic for the wind forecasting track is focused on mimicking the operation 48-hour ahead prediction of hourly power generation at 7 wind farms, based on historical measurements and additional wind forecast information (48-hour ahead predictions of wind speed and direction at the sites). The data is available for period ranging from the 1st hour of 2009/7/1 to the 12th hour of 2012/6/28.

The period between 2009/7/1 and 2010/12/31 is a model identification and training period, while the remainder of the dataset, that is, from 2011/1/1 to 2012/6/28, is there for the evaluation. The training period is there to be used for designing and estimating models permiting to predicting wind power generation at lead times from 1 to 48 hours ahead, based on past power observations and/or available meteorological wind forecasts for that period. Over the evaluation part, it is aimed at mimicking real operational conditions. For that, a number of 48-hour periods with missing power observations where defined. All these power observations are to be predicted. These periods are defined as following. The first period with missing observations is that from 2011/1/1 at 01:00 until 2011/1/3 at 00:00. The second period with missing observations is that from 2011/1/4 at 13:00 until 2011/1/6 at 12:00. Note that to be consistent, only the meteorological forecasts for that period that would actually be available in practice are given. These two periods then repeats every 7 days until the end of the dataset. Inbetween periods with missing data, power observations are available for updating the models.